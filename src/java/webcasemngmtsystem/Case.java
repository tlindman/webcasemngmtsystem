package webcasemngmtsystem;

/**
 *
 * @author Tommy och Robin
 */
public class Case {
    private int id;
    private String description;
    private String department;
    private String totalCost;
    private String totalHours;
    private String actualCost;
    private String actualHours;
    private int stage;
    private boolean editable;
    
    public Case(int id, String description, String department, String totalCost, String totalHours,
            String actualCost, String actualHours, int stage) {
        this.id = id;
        this.description = description;
        this.department = department;
        this.totalCost = totalCost;
        this.totalHours = totalHours;
        this.actualCost = actualCost;
        this.actualHours = actualHours;
        this.stage = stage;
    }
    
    public Case() {
        
    }
    
    public int getId() {
        return this.id;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDepartment() {
        return this.department;
    }
    
    public String getTotalCost() {
        return this.totalCost;
    }
    
    public String getTotalHours() {
        return this.totalHours;
    }
    
    public String getActualCost() {
        return this.actualCost;
    }
    
    public void setActualCost(String actualCost) {
        this.actualCost = actualCost;
    }
    
    public String getActualHours() {
        return this.actualHours;
    }
    
    public void setActualHours(String actualHours) {
        this.actualHours = actualHours;
    }
    
    public int getStage() {
        return this.stage;
    }
    
    public void setStage(int stage) {
        this.stage = stage;
    }
    
    
    //Theese two methods can be deleted?
    public boolean isEditable() {
        return editable;
    }
 
    public void setEditable(boolean editable) {
        this.editable = editable;
    }
    
}
