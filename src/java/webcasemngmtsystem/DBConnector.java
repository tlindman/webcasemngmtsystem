package webcasemngmtsystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tommy och Robin
 */
public class DBConnector {
    
    public static Connection getConnection() throws SQLException
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        String connStr = "jdbc:mysql://localhost:3306/usermanagementdb";
        String userName = "netbeans";
        String password = "netbeans";
        return DriverManager.getConnection(connStr, userName, password);
    }
}
