package webcasemngmtsystem;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Tommy
 */
@ManagedBean
@RequestScoped
public class LoginBean {

    private String username;
    private String password;
    private String errorLoginMessage2 = "";

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    public String tryLogin() {
        Connection con = null;
        Statement stmt = null;
        try {
            con = DBConnector.getConnection();
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from users where username = '"
                    + username + "' and password = '" + password + "'");
            if (rs.next()) {
                if (rs.getString("role").equals("Technician")) {
                    User.setUser(rs);
                    return "main";
                } else
                    setErrorLoginMessage2("You are not a valid user");
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }
        return "index";
    }

    public String getErrorLoginMessage() {
        if (username != null && password != null) {
            return "User: " + username + " with password: " + password
                    + " was not found";
        } else {
            return "";
        }
    }

    public void setErrorLoginMessage2(String msg) {
        this.errorLoginMessage2 = msg;
    }
    public String getErrorLoginMessage2() {
        return this.errorLoginMessage2;
    }
}
