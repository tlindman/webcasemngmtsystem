/*
 * Här laddas ett case vid knapptryck(Edit)
 * och när man registrerar ett nytt case
 * Dessa cases kommer också att sparas här
 */
package webcasemngmtsystem;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Tommy och Robin
 */
@ManagedBean
@SessionScoped
public class ManageCaseBean {

    Connection con = null;
    Statement stmt = null;
    private Case caseToEdit;

    public Case getCaseToEdit() {
        return this.caseToEdit;
    }

    public int getCaseId() {
        return caseToEdit.getId();
    }

    public String getDepartment() {
        return caseToEdit.getDepartment();
    }

    public String getTotalCost() {
        return caseToEdit.getTotalCost();
    }

    public String getTotalHours() {
        return caseToEdit.getTotalHours();
    }

    public String getDescription() {
        return caseToEdit.getDescription();
    }

    public void setDescription(String description) {
        caseToEdit.setDescription(description);
    }

    public String getActualCost() {
        return caseToEdit.getActualCost();
    }

    public void setActualCost(String actualCost) {
        caseToEdit.setActualCost(actualCost);
    }

    public String getActualHours() {
        return caseToEdit.getActualHours();
    }

    public void setActualHours(String actualHours) {
        caseToEdit.setActualHours(actualHours);
    }

    public String edit(Case caseToEdit) {
        this.caseToEdit = caseToEdit;
        return "statetimeandcost";
    }

    public String newCase() {
        this.caseToEdit = new Case();
        return "addcase";
    }

    public String saveCase() {
        String query;
        if (caseToEdit.getId() < 1) {
            //System.out.println("Spara nytt case här: " + caseToEdit.getDescription());
            String sqlValues = "('" + User.department + "','" + caseToEdit.getDescription()
                    + "','" + User.username + "',1)";
            query = "INSERT INTO cases (department,description,author,stage) VALUES " + sqlValues;
        } else {
            //As of now we have costs and hours saved as string in the 
            //database so we have to parse them to Integers
            int totCost = Integer.parseInt(caseToEdit.getTotalCost());
            int totHours = Integer.parseInt(caseToEdit.getTotalHours());
            int actCost = Integer.parseInt(caseToEdit.getActualCost());
            int actHours = Integer.parseInt(caseToEdit.getActualHours());
            if (actCost <= totCost && actHours <= totHours) {
                caseToEdit.setStage(4);
            } else {
                caseToEdit.setStage(3);
            }
            query = "UPDATE cases SET actualcost='" + caseToEdit.getActualCost() + "', actualhours='"
                    + caseToEdit.getActualHours() + "', stage=" + caseToEdit.getStage() + " WHERE id=" + caseToEdit.getId();
        }
        try {
            con = DBConnector.getConnection();
            stmt = con.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException ex) {
            Logger.getLogger(ManageCaseBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }
        return "main";
    }

    // This method is not used any more
    public String getTemporaryString() {
        return "<p style=\"background-color:green;width:150px;\">Stage: " + caseToEdit.getStage() + "</p>";
    }
}
