/*
 * This bean are supposed to handle everything that has
 * to do with a users cases, get cases.
 */
package webcasemngmtsystem;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.sql.rowset.CachedRowSet;

/**
 *
 * @author Tommy och Robin
 */
@ManagedBean
@SessionScoped
public class ManageCasesBean {

    Connection con = null;
    Statement stmt = null;
    private List<Case> cases;
    private int caseId;
    private Case caseToEdit;
    //Variables reguired for saving a new case by a technician
    private String description;
    
    public Case getCaseToEdit() {
        return this.caseToEdit;
    }
    public int getCaseId() {
        return this.caseId;
    }
    
    public void setCaseId(int caseId) {
        this.caseId = caseId;
    }

    public String getUserFullName() {
        return User.fullName;
    }

    public String getUserDepartment() {
        return User.department;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    /*
    public ResultSet getCases() {
        try {
            con = DBConnector.getConnection();
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from cases where assignedTechnician = '"
                    + User.fullName + "' and stage = 2");
            CachedRowSet rowSet = new com.sun.rowset.CachedRowSetImpl();
            rowSet.populate(rs);
            return rowSet;
        } catch (SQLException ex) {
            Logger.getLogger(ManageCasesBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }
        return null;
    }
    */

    public List<Case> getCases2() {
        Case oneCase;
        cases = new ArrayList<Case>();
        try {
            con = DBConnector.getConnection();
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from cases where assignedTechnician = '"
                    + User.fullName + "' and stage = 2");
            while(rs.next()) {
                oneCase = new Case(rs.getInt("id"),rs.getString("description"),rs.getString("department"),
                        rs.getString("totalcost"),rs.getString("totalhours"),rs.getString("actualcost"),
                        rs.getString("actualhours"),rs.getInt("stage"));
                cases.add(oneCase);
            }
            return cases;
        } catch (SQLException ex) {
            Logger.getLogger(ManageCasesBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }
        return null;
    }

    //This is not used anymore
    /*
    public String save() {
        String sqlValues = "('" + User.department + "','" + getDescription()
                + "','" + User.username + "',1)";
        String query = "INSERT INTO cases (department,description,author,stage) VALUES " + sqlValues;
        try {
            con = DBConnector.getConnection();
            stmt = con.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException ex) {
            Logger.getLogger(ManageCasesBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }
        return "main";
    }
    */

    //This is not used anymore
    /*
    public String edit(Case caseToEdit) {
        this.caseToEdit = caseToEdit;
        return "statetimeandcost";
    }
    
    public String getDesc() {
        return caseToEdit.getDescription();
    }
    */
}
