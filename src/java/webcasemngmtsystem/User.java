package webcasemngmtsystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tommy och Robin
 */
public class User {
    public static String firstname;
    public static String lastname;
    public static String username;
    public static String password;
    public static String role;
    public static String department;
    
    public static String fullName;
    
    public static void setUser(ResultSet user) {
        try {
            firstname = user.getString("firstname");
            lastname = user.getString("lastname");
            username = user.getString("username");
            password = user.getString("password");
            role = user.getString("role");
            department = user.getString("department");
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        fullName = firstname + " " + lastname;
        
    }
}
